package ch.zli.api16a.m226a.utils;

import ch.zli.api16a.m226a.stacklib.impl.StackList;


public class Main {
    public static void main(String[] arg) {
        StackList<String> theStack;

        theStack = new StackList<String>();

        theStack.push("jaja");
        theStack.push("blabla");

        for (int i = 0; i < 20; i++) {
            theStack.push("my item "+i);
        }
        System.out.println(theStack.size());

        theStack.printAllElements();
    }
}
