/*
 *
 */
package ch.zli.api16a.m226a.stacklib.impl;

import ch.zli.api16a.m226a.stacklib.Stack;
import ch.zli.api16a.m226a.stacklib.StackIsEmptyException;
import ch.zli.api16a.m226a.stacklib.StackIsFullException;

// TODO: Auto-generated Javadoc
/**
 * The Class StackArray.
 *
 * @param <T> the generic type
 */
public class StackArray<T> implements Stack<T> {

	/** The stack. */
	private T[] stack;

	/** The max height. */
	private int maxHeight;

	/** The height. */
	private int height;

	/**
	 * Instantiates a new stack array.
	 */
	public StackArray() {
		this(10);
	}

	/**
	 * Instantiates a new stack array with stackHeight parameter
	 *
	 * @param stackHeight the stack height
	 */
	public StackArray(int stackHeight) {
		maxHeight = stackHeight-1;
		height = -1;
		stack = (T[])new Object[stackHeight];
	}

	/* (non-Javadoc)
	 * @see ch.zli.api16a.m226a.stacklib.Stack#push(java.lang.Object)
	 */
	@Override
	public void push(T item) throws StackIsFullException {
		if(height == maxHeight) {
			throw new StackIsFullException();
		}
		stack[++height] = item;
	}

	/* (non-Javadoc)
	 * @see ch.zli.api16a.m226a.stacklib.Stack#pop()
	 */
	@Override
	public T pop() throws StackIsEmptyException {
		if(height < 0) {
			throw new StackIsEmptyException();
		}
		return stack[height--];
	}

	/* (non-Javadoc)
	 * @see ch.zli.api16a.m226a.stacklib.Stack#size()
	 */
	@Override
	public int size() {
		return height;
	}

	/* (non-Javadoc)
	 * @see ch.zli.api16a.m226a.stacklib.Stack#isFull()
	 */
	@Override
	public boolean isFull() {
		return height >= maxHeight;
	}

	/* (non-Javadoc)
	 * @see ch.zli.api16a.m226a.stacklib.Stack#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return height < 0;
	}

	@Override
	public T peek(int position) throws StackIsEmptyException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void printAllElements() {
		
	}
}
