package ch.zli.api16a.m226a.stacklib.impl;

@SuppressWarnings("serial")
public class StackIndexException extends RuntimeException {
	public StackIndexException() {
		super("");
	}
	public StackIndexException(String message) {
		super(message);
	}
}
