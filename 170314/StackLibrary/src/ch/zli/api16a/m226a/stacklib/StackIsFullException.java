/*
 * 
 */
package ch.zli.api16a.m226a.stacklib;


/**
 * The Class StackIsFullException.
 */
@SuppressWarnings("serial")
public class StackIsFullException extends RuntimeException {

	/**
	 * Instantiates a new stack is full exception.
	 */
	public StackIsFullException() {
		super();
	}

	/**
	 * Instantiates a new stack is full exception.
	 *
	 * @param message the message
	 */
	public StackIsFullException(String message) {
		super(message);
	}

}
