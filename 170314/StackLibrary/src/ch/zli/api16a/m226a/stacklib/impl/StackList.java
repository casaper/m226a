/*
 *
 */
package ch.zli.api16a.m226a.stacklib.impl;

import java.util.ArrayList;

import ch.zli.api16a.m226a.stacklib.Stack;
import ch.zli.api16a.m226a.stacklib.StackIsEmptyException;


/**
 * The Class StackList.
 *
 * @param <T> stack for generic type items
 */
public class StackList<T> implements Stack<T> {

	/** The stack. */
	private ArrayList<T> stack;

	/**
	 * Instantiates a new stack list.
	 */
	public StackList() {
		stack = new ArrayList<T>();
	}

	/* (non-Javadoc)
	 * @see ch.zli.api16a.m226a.stacklib.Stack#push(java.lang.Object)
	 */
	@Override
	public void push(T item) {
		stack.add(item);
	}

	/* (non-Javadoc)
	 * @see ch.zli.api16a.m226a.stacklib.Stack#pop()
	 */
	@Override
	public T pop() throws StackIsEmptyException {
		if(size() < 1) {
			throw new StackIsEmptyException();
		}
		return stack.remove(size() - 1);
	}

	/* (non-Javadoc)
	 * @see ch.zli.api16a.m226a.stacklib.Stack#size()
	 */
	@Override
	public int size() {
		return stack.size();
	}

	/* (non-Javadoc)
	 * @see ch.zli.api16a.m226a.stacklib.Stack#isFull()
	 */
	@Override
	public boolean isFull() {
		return false;
	}

	/* (non-Javadoc)
	 * @see ch.zli.api16a.m226a.stacklib.Stack#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return stack.isEmpty();
	}


	/* (non-Javadoc)
	 * @see ch.zli.api16a.m226a.stacklib.Stack#peek(int)
	 */
	@Override
	public T peek(int position) throws StackIndexException {
		if(stack.isEmpty()) throw new StackIndexException();
		return stack.get(position);
	}

    @Override
    public void printAllElements() {
        for (int i = 0; i < this.size(); i++) {
            System.out.println(this.peek(i));
        }
    }
}
