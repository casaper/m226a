/*
 *
 */
package ch.zli.api16a.m226a.stacklib;


/**
 * The Interface Stack.
 *
 * @param <T> Stack for generic type items
 */
public interface Stack<T> {

	/**
	 * Pushes item on top of the stack.
	 *
	 * @param item the item
	 * @throws StackIsFullException on push to full stack
	 */
	public void push(T item) throws StackIsFullException;

	/**
	 * Pops item from top of stack.
	 *
	 * @return item of type T from top of stack
	 * @throws StackIsEmptyException on pop() of empty stack
	 */
	public T pop() throws StackIsEmptyException;

	/**
	 * Get the size of the stack.
	 *
	 * @return integer with size of the stack
	 */
	public int size();

	/**
	 * Check if stack is full.
	 *
	 * @return true if stack is full
	 */
	public boolean isFull();

	/**
	 * Get stack empty state.
	 *
	 * @return true if stack is empty
	 */
	public boolean isEmpty();


	/**
	 * Gets the item from position, but doesn't remove it from the stack
	 * @param position to peek
	 * @return copy of item from the Stack
	 */
	public T peek(int position) throws StackIsEmptyException;

	/**
	 * Prints all elements to the terminal
	 */
	public void printAllElements();
}
