/*
 * 
 */
package ch.zli.api16a.m226a.stacklib;


/**
 * The Class StackIsEmptyException.
 */
@SuppressWarnings("serial")
public class StackIsEmptyException extends RuntimeException {

	/**
	 * Instantiates a new stack is empty exception.
	 */
	public StackIsEmptyException() {
		super();
	}
	
	/**
	 * Instantiates a new stack is empty exception.
	 *
	 * @param message the exception message string
	 */
	public StackIsEmptyException(String message) {
		super(message);
	}
}
