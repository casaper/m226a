/*
 * 
 */
package ch.zli.api16a.m226a.stacklib.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ch.zli.api16a.m226a.stacklib.StackIsEmptyException;


/**
 * The Class: StackListTest.
 */
public class StackListTest {
	
	/** The stack. */
	// Object under Test
	private StackList<String> stack;

	/**
	 * Instantiate the Test subject
	 */
	@Before // Called before a test function runs
	public void init() {
		stack = new StackList<String>(); // Initialize test object
	}

	/**
	 * Constructor test.
	 */
	@Test
	public void constructorTest() {
		assertEquals(0, stack.size());
		assertTrue(stack.isEmpty());
	}
	
	/**
	 * Push test.
	 */
	@Test
	public void pushTest() {
		assertEquals(0 ,stack.size());
		stack.push("teststring");
		assertFalse(stack.isEmpty());
	}
	
	/**
	 * Pop test.
	 */
	@Test
	public void popTest() {
		stack.push("a string");
		assertFalse(stack.isEmpty());
		assertEquals("a string", stack.pop());
	}
	
	/**
	 * Empty exception test.
	 */
	@Test(expected=StackIsEmptyException.class)
	public void emptyExceptionTest() {
		assertTrue(stack.isEmpty());
		stack.pop();
	}
	
	@Test
	public void peekStackItemTest() {
		stack.push("test string");
		assertFalse(stack.isEmpty());
		assertEquals("test string", stack.peek(stack.size()-1));
		assertFalse(stack.isEmpty());	
	}
	
	@Test(expected=StackIsEmptyException.class)
	public void peekEmptyStackTest() {
		assertTrue(stack.isEmpty());
		stack.peek(0);
	}
}
