/*
 * 
 */
package ch.zli.api16a.m226a.stacklib.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ch.zli.api16a.m226a.stacklib.StackIsEmptyException;
import ch.zli.api16a.m226a.stacklib.StackIsFullException;


/**
 * The Class StackArrayTest.
 */
public class StackArrayTest {
	
	/** The stack. */
	private StackArray<String> stack;
	
	/** The no param stack. */
	private StackArray<String> noParamStack;
	
	/** The stack param height. */
	private int stackParamHeight;
	
	/**
	 * Inits the.
	 */
	@Before
	public void init(){
		stackParamHeight = 15;
		stack = new StackArray<String>(stackParamHeight);
		noParamStack = new StackArray<String>();
	}

	/**
	 * Stack array construct test.
	 */
	@Test
	public void stackArrayConstructTest() {
		assertEquals(-1, stack.size());
		assertTrue(stack.isEmpty());
	}
	
	/**
	 * Construct no param test.
	 */
	@Test
	public void constructNoParamTest() {
		assertEquals(-1, noParamStack.size());
		assertTrue(noParamStack.isEmpty());
	}
	
	/**
	 * Stack order test.
	 */
	@Test
	public void stackOrderTest() {
		for (int i = 0; i < stackParamHeight; i++) {
			stack.push("test item"+i);
		}
		for (int i = stackParamHeight-1; i > -1; i--) {
			assertEquals("test item"+i, stack.pop());
		}
	}
	
	/**
	 * Push item test.
	 */
	@Test
	public void pushItemTest() {
		stack.push("mystring");
		assertEquals(0, stack.size());
		assertFalse(stack.isEmpty());
	}
	
	/**
	 * Checks if is stack full test.
	 */
	@Test
	public void isStackFullTest() {
		for (int i = 0; i < stackParamHeight; i++) {
			stack.push("string item " + i);
		}
		assertTrue(stack.isFull());
	}
	
	/**
	 * Pop test.
	 */
	@Test
	public void popTest() {
		stack.push("a test string");
		assertEquals("a test string", stack.pop());
		assertEquals(-1, stack.size());
	}
	
	/**
	 * Pop on empty stack test.
	 */
	@Test(expected=StackIsEmptyException.class)
	public void popOnEmptyStackTest() {
		assertEquals(-1, stack.size());
		stack.pop();
	}
	
	/**
	 * Checks if is stack too full test.
	 */
	@Test(expected=StackIsFullException.class)
	public void isStackTooFullTest() {
		for (int i = 0; i < stackParamHeight+1; i++) {
			stack.push("string item " + i);
		}
	}
}
