package ch.zli.m226.queue.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import ch.zli.m226.queue.Queue;

/**
 * Test QueueArray functionality
 */
public class QueueArrayListTest {

	private Queue<String> queue;
	
	@Before
	public void initTest() {
		queue = new QueueArray<String>();
	}
	
	@Test
	/** A new instance is empty*/
	public void testNewQueueIsEmpy() {
		assertTrue(queue.isEmpty());
	}
	
	@Test
	/** After inserting and removing an element, the queue is empty */
	public void testIsEmptyTest() {
		for (int i = 0; i < 2 * QueueArray.MAX_LENGHT; ++i) {
			queue.add("one");
			queue.remove();
		}
		assertTrue(queue.isEmpty());
	}
	
	@Test
	/** A new instance is not full */
	public void newQueueIsNotFull() {
		assertFalse(queue.isFull());
	}
	
	@Test
	/** After inserting the maximal numbers of elements, the queue is full */
	public void testIsFull() {
		for (int i = 0; i < QueueArray.MAX_LENGHT; ++i) {
			queue.add("item" + i);
		}
		assertTrue(queue.isFull());
	}
	
	@Test
	/** 
	 * If we insert n elements, we can remove n elements
	 * The insert and remove sequence must be equal
	 * After removing n elements, the queue must be empty
	 */
	public void testAddRemove() {
		for (int i = 0; i < QueueArray.MAX_LENGHT; ++ i) {
			queue.add("item" + i);
		}
		for (int i = 0; i < QueueArray.MAX_LENGHT; ++ i) {
			String item = queue.remove();
			assertEquals("item" + i, item);
		}
		assertTrue(queue.isEmpty());
	}
	
	@Test(expected = QueueEmptyException.class)
	/** Removing an element from an empty queue must throw an QueueEmptyException */
	public void testEmptyException() {
		queue.remove();
	}
	
	@Test(expected = QueueFullException.class)
	/** Over filling the queue must throw an QueueFullException */
	public void testFullException() {
		for (int i = 0; i <= QueueArray.MAX_LENGHT; ++i) {
			queue.add("item");
		}
	}

}
