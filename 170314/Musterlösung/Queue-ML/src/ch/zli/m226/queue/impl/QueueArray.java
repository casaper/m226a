package ch.zli.m226.queue.impl;

import ch.zli.m226.queue.Queue;

/**
 * Implements a Queue with a Java array. Size is fixed.
 *
 * @param <T> The item type to be used with the Queue
 */
public class QueueArray<T> implements Queue<T> {

	protected static final int MAX_LENGHT = 10;

	private T[] data;
	private int nextPos;  // next insert position
	private int quantity; // number of items in the queue

	@SuppressWarnings("unchecked")
	public QueueArray() {
		data = (T[]) (new Object[MAX_LENGHT]);
		nextPos = 0;
		quantity = 0;
	}

	@Override
	public void add(T item) {
		if (isFull()) {
			throw new QueueFullException();
		}
		data[nextPos] = item;
		nextPos = (nextPos + 1) % MAX_LENGHT;
		++quantity;
	}

	@Override
	public T remove() 																						{
		if (isEmpty()) {
			throw new QueueEmptyException();
		}
		T result = data[((nextPos - quantity) + MAX_LENGHT) % MAX_LENGHT];
		--quantity;
		return result;
	}

	@Override
	public boolean isEmpty() {
		return quantity == 0;
	}

	@Override
	public boolean isFull() {
		return quantity == MAX_LENGHT;
	}
}
