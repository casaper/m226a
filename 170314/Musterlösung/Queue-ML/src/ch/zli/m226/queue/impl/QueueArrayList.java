package ch.zli.m226.queue.impl;

import java.util.ArrayList;

import ch.zli.m226.queue.Queue;

/**
 * Implements a Queue with an AarrayList.
 *
 * @param <T> The item type to be used with the Queue
 */
public class QueueArrayList<T> implements Queue<T> {

	private ArrayList<T> data;
	
	public QueueArrayList() {
		data = new ArrayList<T>();
	}
	
	@Override
	public void add(T item) {
		data.add(item);		
	}

	@Override
	public T remove() {
		if (isEmpty()) {
			throw new QueueEmptyException();
		}
		return data.remove(0);
	}

	@Override
	public boolean isEmpty() {
		return data.isEmpty();
	}

	@Override
	public boolean isFull() {
		return false; // Never ever full
	}

}
