package ch.zli.m226.queue;

/**
 * Queue functionality</p>
 * Queue should be implemented with FIFO (first in first out) semantics
 *
 * @param <T> The item type to be used with the Queue
 */
public interface Queue<T> {
	
	/**
	 * Add an item to the end of the queue
	 * @param item the item to be added
	 */
	public void add(T item);
	
	/**
	 * Remove an item at the front of the queue<p/>
	 * Queue must not be empty or an Exception will be thrown
	 * @return the front item
	 */
	public T remove();
	
	/**
	 * @return true if queue is empty, false otherwise
	 */
	public boolean isEmpty();
	
	/**
	 * @return true if queue is full, false otherwise
	 */
	public boolean isFull();
}
