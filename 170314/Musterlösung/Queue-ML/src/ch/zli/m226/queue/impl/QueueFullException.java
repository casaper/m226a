package ch.zli.m226.queue.impl;

/**
 * Exception to be thrown if a Queue is full and an add() is performed
 */
@SuppressWarnings("serial")
public class QueueFullException extends RuntimeException {
	public QueueFullException() {
		super("add to a full queue not allowed");
	}
	public QueueFullException(String msg) {
		super(msg);
	}
}
