package ch.zli.m226.queue;

import ch.zli.m226.queue.impl.QueueArray;
import ch.zli.m226.queue.impl.QueueEmptyException;

/** Main Klass for a proof of concept test */
public class Main {
	/**
	 * Program entry point
	 * @param args not used
	 */
	public static void main(String[] args) {
		Queue<Person> personQueue = new QueueArray<>();
		personQueue.add(new Person("Max"));
		personQueue.remove();
		try {
			Person customer = personQueue.remove();
			System.out.println("Serving the customers demands for: " + customer.getName());
		} catch (QueueEmptyException e) {
			System.out.println("Gone fishing");
		}
	}
}
