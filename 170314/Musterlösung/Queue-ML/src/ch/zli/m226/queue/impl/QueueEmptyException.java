package ch.zli.m226.queue.impl;

/**
 * Exception to be thrown if a Queue is empty and a remove() is performed
 */
@SuppressWarnings("serial")
public class QueueEmptyException extends RuntimeException {
	public QueueEmptyException() {
		super("remove on an empty queue not allowed");
	}
	public QueueEmptyException(String msg) {
		super(msg);
	}
}
