package ch.zli.m226.queue;

/**
 * A simple Person to be used with a Queue
 */
public class Person {
	private String name;
	
	/**
	 * Constructor
	 * @param fullName the persons name
	 */
	public Person(String fullName) {
		this.name = fullName;
	}

	/**
	 * @return the persons name
	 */
	public String getName() {
		return name;
	}
}
