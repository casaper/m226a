package ch.zli.m226.stack.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import ch.zli.m226.stack.Stack;

/**
 * Test StackArrayListTest functionality
 */
public class StackArrayListTest {

	private Stack<String> stack;
	
	@Before
	public void initTest() {
		stack = new StackArrayList<String>();
	}
	
	@Test
	// A new Instance must be empty
	public void testNewQueueIsEmpy() {
		assertTrue(stack.isEmpty());
	}
	
	@Test
	// After a push() and a pop() it must be empty
	public void testIsEmptyTest() {
		for (int i = 0; i < 2 * StackArray.MAX_LENGHT; ++i) {
			stack.push("one");
			stack.pop();
		}
		assertTrue(stack.isEmpty());
	}
	
	@Test
	// A new stack must not be full
	public void newStackIsNotFull() {
		assertFalse(stack.isFull());
	}
	
	@Test
	// Stack is never full
	public void testIsFull() {
		assertFalse(stack.isFull());
	}
		
	@Test
	// After pushing n elements, n elements can be removed
	// The resulting order is reversed
	// Afterwards, the stack is empty
	public void testAddRemove() {
		for (int i = 0; i < StackArray.MAX_LENGHT; ++ i) {
			stack.push("item" + i);
		}
		for (int i = 0; i < StackArray.MAX_LENGHT; ++ i) {
			String item = stack.pop();
			assertEquals("item" + (StackArray.MAX_LENGHT - i - 1), item);
		}
		assertTrue(stack.isEmpty());
	}
	
	@Test(expected = StackEmptyException.class)
	// pop() on an empty stack must throw an exception
	public void testEmptyException() {
		stack.pop();
	}
		
	@Test
	// Peek has to yield the right value
	public void testPeek() {
		stack.push("item0");
		stack.push("item1");
		stack.push("item2");
		
		assertEquals(stack.peek(0), "item2");
		assertEquals(stack.peek(1), "item1");
		assertEquals(stack.peek(2), "item0");
	}
	
	@Test(expected = StackIndexException.class)
	// peeking at an non existing position must throw an exception
	public void testPeekException() {
		stack.peek(0); // Stack empty, must fail;
	}
}
