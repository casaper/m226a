package ch.zli.m226.stack.impl;

/**
 * Exception to be thrown if a Stack is full and a push() is performed
 */
@SuppressWarnings("serial")
public class StackFullException extends RuntimeException {
	public StackFullException() {
		super("Push on a full Stack not allowed");
	}
	public StackFullException(String msg) {
		super(msg);
	}
}
