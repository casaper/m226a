package ch.zli.m226.stack.impl;

/**
 * Exception to be thrown if a peek() is performed with an out of bounds position
 */
@SuppressWarnings("serial")
public class StackIndexException extends RuntimeException {
	public StackIndexException() {
		super("Peek position is out of bounds");
	}
	public StackIndexException(String msg) {
		super(msg);
	}
}
