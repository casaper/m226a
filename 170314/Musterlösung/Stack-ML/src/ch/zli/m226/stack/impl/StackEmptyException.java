package ch.zli.m226.stack.impl;

/**
 * Exception to be thrown if a Stack is empty and a pop() is performed
 */
@SuppressWarnings("serial")
public class StackEmptyException  extends RuntimeException {
	public StackEmptyException() {
		super("Pop on an empty Stack not allowed");
	}
	public StackEmptyException(String msg) {
		super(msg);
	}
}
