package ch.zli.m226.stack.impl;

import java.util.ArrayList;

import ch.zli.m226.stack.Stack;

/**
 * Implements a Stack with an ArrayList
 *
 * @param <T> The item type to be used with the stack
 */
public class StackArrayList<T> implements Stack<T> {

	private ArrayList<T> data;
	
	public StackArrayList() {
		data = new ArrayList<T>();
	}
	
	@Override
	public void push(T item) {
		data.add(0, item);
	}

	@Override
	public T pop() {
		if (isEmpty()) {
			throw new StackEmptyException();
		}
		return data.remove(0);
	}

	@Override
	public boolean isEmpty() {
		return data.isEmpty();
	}

	@Override
	public boolean isFull() {
		return false;
	}

	@Override
	public T peek(int position) {
		if (position < 0 || position >= data.size()) {
			throw new StackIndexException();
		}
		return data.get(position);
	}

	@Override
	public int size() {
		return data.size();
	}
}
