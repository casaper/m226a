package ch.zli.m226.stack.impl;

import ch.zli.m226.stack.Stack;

/**
 * Implements a Stack with an Java array so it is fix in size
 *
 * @param <T> The item type to be used with the stack
 */
public class StackArray<T> implements Stack<T> {

	protected static final int MAX_LENGHT = 10;
	
	private T[] data;
	private int nextPos;
	
	@SuppressWarnings("unchecked")
	public StackArray() {
		nextPos = 0;
		data = (T[])(new Object[MAX_LENGHT]);
	}
	
	@Override
	public void push(T item) {
		if (isFull()) {
			throw new StackFullException();
		}
		data[nextPos] = item;
		++nextPos;
	}

	@Override
	public T pop() {
		if (isEmpty()) {
			throw new StackEmptyException();
		}
		--nextPos;
		return data[nextPos];
	}

	@Override
	public boolean isEmpty() {
		return nextPos == 0;
	}

	@Override
	public boolean isFull() {
		return nextPos >= MAX_LENGHT;
	}

	@Override
	public T peek(int position) {
		if (position < 0 || position >= size()) {
			throw new StackIndexException();
		}
		return data[nextPos-position-1];
	}

	@Override
	public int size() {
		return nextPos;
	}
}
