package ch.zli.m226.stack;

/**
 * Stack functionality</p>
 * Should be implemented with FIFO (first in first out) semantics
 *
 * @param <T> The item type to be used with the stack
 */
public interface Stack<T> {

	/**
	 * Push an item to the top of the stack
	 * @param item
	 */
	public void push(T item);
	
	/**
	 * Pop an item from the top of the stack
	 * @return
	 */
	public T pop();
	
	/**
	 * @return true if stack is empty, false otherwise
	 */
	public boolean isEmpty();
	
	/**
	 * @return true if stack is full, false otherwise
	 */
	public boolean isFull();
	
	/**
	 * Peek at the item at the given position.
	 * @param position position, zero based for top item
	 * @return the item at the given position
	 */
	public T peek(int position);
	
	/**
	 * @return the number of items on the stack
	 */
	public int size();
}
