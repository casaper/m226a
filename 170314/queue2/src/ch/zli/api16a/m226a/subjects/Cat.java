package ch.zli.api16a.m226a.subjects;

public class Cat {
	/**
	 * Field Cats name
	 */
	private String name;
	
	public Cat(String newName) {
		name = newName;
	}
	
	public String getName() {
		return name;
	}
	
}
