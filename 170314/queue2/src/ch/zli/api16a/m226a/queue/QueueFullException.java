package ch.zli.api16a.m226a.queue;

@SuppressWarnings("serial")
public class QueueFullException extends RuntimeException {
	public QueueFullException() {super();}
	public QueueFullException(String message) {
		super(message);
	}
}
