package ch.zli.api16a.m226a.queue.utils;

import ch.zli.api16a.m226a.queue.impl.QueueList;
import ch.zli.api16a.m226a.subjects.Cat;

public class Main {
	public static void main(String[] args) {
/*		QueueArray<Cat> cats; //(1) Declaration
		cats = // (2) assignment 
			   new QueueArray<Cat>(20); // (3) object instantiation
//		cats.add(new Cat("Ronny"));
//		System.out.println(cats.size());
//		cats.add(new Cat("Garfield"));

//		System.out.println(cats.size());
//		
//		Cat cat = cats.remove();
//
//		System.out.println(cat.getName());
//
//		System.out.println(cats.size());
//		
//		Cat cat2 = cats.remove();
//
//		System.out.println(cat2.getName());
//		System.out.println(cats.size());
		for (int i = 0; !cats.isFull(); i++) {
			cats.add(new Cat("Büsi"+i));
		}
		
		System.out.println(cats.isFull());
		System.out.println(cats.size());
		
		while(!cats.isEmpty()) {
			Cat res = cats.remove();
			System.out.println(res.getName());
		}*/

		QueueList<Cat> cats;
		cats = new QueueList<Cat>();
		cats.add(new Cat("Garfield"));
		System.out.println(cats.size());

        for (int i = 0; i < 30; i++) {
            cats.add(new Cat("Garfield" + i));
        }
        System.out.println(cats.size());

        while (!cats.isEmpty()) {
            Cat pos = cats.remove();
            System.out.println(pos.getName());
            System.out.println(cats.size());
        }

        try {
            Cat versuch = cats.remove();
        } catch (RuntimeException e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }

    }
}
