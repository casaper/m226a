package ch.zli.api16a.m226a.queue.impl;

import java.util.ArrayList;

import ch.zli.api16a.m226a.queue.Queue;
import ch.zli.api16a.m226a.queue.QueueEmptyException;
import ch.zli.api16a.m226a.queue.QueueFullException;

public class  QueueList<T> implements Queue<T> {

	private ArrayList<T> data;


	public QueueList() {
		data = new ArrayList<>(1);
	}
	
	
	@Override
	public void add(T item) throws QueueFullException {
		data.add(item);
	}

	@Override
	public T remove() throws QueueEmptyException {
		if(data.isEmpty()) throw new QueueEmptyException();
		return data.remove(0);
	}

	@Override
	public boolean isEmpty() {
		return data.isEmpty();
	}

	@Override
	public boolean isFull() {
		return false;
	}

	@Override
	public int size() {
		return data.size();
	}

}
