package ch.zli.api16a.m226a.queue;

@SuppressWarnings("serial")
public class QueueEmptyException extends RuntimeException {
	public QueueEmptyException() {super();}
	public QueueEmptyException(String message) {
		super(message);
	}
}
