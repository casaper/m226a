package ch.zli.api16a.m226a.queue.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ch.zli.api16a.m226a.queue.Queue;
import ch.zli.api16a.m226a.queue.QueueEmptyException;
import ch.zli.api16a.m226a.queue.QueueFullException;

public class QueueArrayTest {
	
	// Object under Test
	private QueueArray<String> queue;

	@Before // Called before a test function runs
	public void init() {
		queue = new QueueArray<String>(); // Initialize test object
	}
	
	@Test // This is a test function
	public void constructorTest() {
		ArrayList<QueueArray<String>> queues = new ArrayList<>();
		queues.add(this.queue);
		queues.add(new QueueArray<String>(42));
		
		for (Queue<String> queue : queues) {
			assertEquals(0, queue.size());
			assertTrue(queue.isEmpty());
			assertFalse(queue.isFull());
		}
	}
	
	@Test
	public void addRemoveTest() {
		String teststring = "my test string is here";
		queue.add(teststring);
		assertEquals(1, queue.size());
		
		String res = queue.remove();
		assertEquals(teststring, res);
		assertEquals(0, queue.size());
	}
	
	@Test
	public void removeExceptionTest1() {
		QueueArray<String> queue = new QueueArray<>();
		try {
			queue.remove();
			// No exception = test failed
			fail("Queue was beeing applyed .remove() on empty queue");
		} catch(QueueEmptyException e) {
			// Test OK
		}
	}
	
	@Test
	public void orderTest() {
		for (int i = 0; i < 5; i++) {
			queue.add("item"+i);
		}
		for (int i = 0; i < 5; i++) {
			assertEquals("item"+i, queue.remove());
		}
	}
	
	@Test(expected=QueueEmptyException.class)
	public void removeExceptionTest0() {
		queue.remove();
	}
	
	@Test(expected=QueueFullException.class)
	public void addExceptionTest() {
		while(!queue.isFull()) {
			queue.add("fridolin");
		}
		
		assertTrue(queue.isFull());
		
		queue.add("päng");
	}
}
