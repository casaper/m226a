package ch.zli.mod226a.api16a.queue;

public class Cat {
	/**
	 * Field Cats name
	 */
	private String name;
	
	public Cat(String newName) {
		name = newName;
	}
	
	public String getName() {
		return name;
	}
	
}
