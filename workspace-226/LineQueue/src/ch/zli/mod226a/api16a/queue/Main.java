package ch.zli.mod226a.api16a.queue;

public class Main {
	public static void main(String[] args) {
		QueueList<Cat> cats;
		cats = new QueueList<Cat>();
		
		cats.add(new Cat("Garfield"));
		cats.add(new Cat("Tom"));
		System.out.println(cats.size());
	}
}
