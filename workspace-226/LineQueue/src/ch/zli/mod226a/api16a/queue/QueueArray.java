package ch.zli.mod226a.api16a.queue;

/**
 * FIFO as array with fixed size
 */
public class QueueArray<T> implements Queue<T> {
	
	// Fields (in Java speak: Property, member, instance variable)
	private T[] data;
	private int nextFreePos;
	private int itemCount;
	private int maxQueueSize;
	
	
	/**
	 * Constructor
	 */
	public QueueArray() {
		this(13);
	}
	
	public QueueArray(int sizeQueue) {
		maxQueueSize = sizeQueue;
		nextFreePos = 0;
		itemCount = 0;
		data = (T[])new Object[maxQueueSize];
	}
	
	@Override
	public void add(T item) throws RuntimeException {
		if(isFull()) {
			throw new RuntimeException();
		}
		data[nextFreePos] = item;
		nextFreePos = (nextFreePos + 1) % maxQueueSize;
		++itemCount;
	}
	
	@Override
	public T remove() throws RuntimeException {
		if(isEmpty()) {
			throw new RuntimeException();
		}
		int removePos = (nextFreePos - itemCount + maxQueueSize) % maxQueueSize;
		--itemCount;
		return data[removePos];
		
	}
	@Override
	public boolean isEmpty() {
		return itemCount == 0;
	}
	@Override
	public boolean isFull() {
		return itemCount >= maxQueueSize;
	}
	@Override
	public int size() {
		return itemCount;
	}
}
