package ch.zli.mod226a.api16a.queue;

@SuppressWarnings("serial")
public class QueueEmptyException extends RuntimeException {
	
	public QueueEmptyException() {
		super();
	}
	
	public QueueEmptyException(String message) {
		super(message);
	}
}
