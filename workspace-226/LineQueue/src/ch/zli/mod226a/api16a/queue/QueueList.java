package ch.zli.mod226a.api16a.queue;

import java.util.ArrayList;

public class  QueueList<T> implements Queue<T> {

	private ArrayList<T> data;


	public QueueList() {
		data = new ArrayList<>(1);
	}
	
	
	@Override
	public void add(T item) throws RuntimeException {
		data.add(item);
	}

	@Override
	public T remove() throws RuntimeException {
		if(data.isEmpty()) throw new RuntimeException();
		return data.remove(0);
	}

	@Override
	public boolean isEmpty() {
		return data.isEmpty();
	}

	@Override
	public boolean isFull() {
		return false;
	}

	@Override
	public int size() {
		return data.size();
	}

}
