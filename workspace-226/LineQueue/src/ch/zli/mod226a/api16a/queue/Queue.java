package ch.zli.mod226a.api16a.queue;

/**
 * A generic queue (FIFO)
 * 
 * @param <T> generic type
 *
 */
public interface Queue<T> {
	
	/**
	 * Add an item at the back of the queue
	 * 
	 * @param item to be added
	 * 
	 * @throws RuntimeException if the queue is full
	 */
	public void add(T item) throws QueueFullException;
	
	/**
	 * Remove an item
	 * 
	 * @return the next item
	 * 
	 * @throws RuntimeException if the queue is empty
	 */
	public T remove() throws QueueEmptyException;
	
	/**
	 * Tells if the queue is empty
	 * @return true if queue is empty
	 */
	public boolean isEmpty();
	
	/**
	 * Tells if the queue is full
	 * @return true if queue is full
	 */
	public boolean isFull();
	
	/**
	 * Tells the number of items present in the queue
	 * @return integer with the number of items
	 */
	public int size();
}
