package ch.zli.mod226a.api16a.queue;

@SuppressWarnings("serial")
public class QueueFullException extends RuntimeException {
	
	public QueueFullException() {
		super();
	}
	
	public QueueFullException(String message) {
		super(message);
	}
}
